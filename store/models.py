from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=250, verbose_name='Название')

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=250, verbose_name='Название')
    description = models.TextField(verbose_name='Описание')
    category = models.ForeignKey(to=Category, on_delete=models.CASCADE, verbose_name='Категория')
    price = models.DecimalField(max_digits=20, decimal_places=2, verbose_name='Цена')

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    def __str__(self):
        return f'{self.name} - {self.price}'


class Image(models.Model):
    image_file = models.ImageField(verbose_name='Фотография')
    product = models.ForeignKey(to=Product, on_delete=models.CASCADE, verbose_name='Товар')

    class Meta:
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотографии'

    def __str__(self):
        return f'Фотография: {self.product}'
