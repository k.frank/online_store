from django.urls import path, include

from store.views import CategoryListView, CategoryDetailView, ProductListView, ProductDetailView

urlpatterns = [
    path('categories/', CategoryListView.as_view()),
    path('categories/<int:pk>/', CategoryDetailView.as_view()),
    path('products/', ProductListView.as_view()),
    path('products/<int:pk>/', ProductDetailView.as_view()),

    path('api/', include('store.api.urls')),
]
