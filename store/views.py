from django.views.generic import ListView, DetailView

from store.models import Category, Product, Image


class CategoryListView(ListView):
    model = Category


class CategoryDetailView(DetailView):
    queryset = Category.objects.all()
    context_object_name = 'category'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['product_list'] = Product.objects.filter(category=self.object)
        return context


class ProductListView(ListView):
    model = Product


class ProductDetailView(DetailView):
    queryset = Product.objects.all()
    context_object_name = 'product'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['image_list'] = Image.objects.filter(product=self.object)
        return context
